import { useState } from 'react'
import reactLogo from './assets/react.svg'
import viteLogo from '/vite.svg'
import './App.css'
import React from "react";


function ProdutosTable(props) {
  const { list, onDismiss, searchTerm} = props;
  return (
      <table>
        <thead>
          <tr>
            <th>Título</th>
            <th>Ano</th>
            <th>URL do Poster</th>
          </tr>
        </thead>
        <tbody>
          {list.filter(produto => produto.name.toLowerCase().includes(searchTerm.toLowerCase()))
          .map((produto) => (
            <tr key={produto.id}>
              <td>{produto.name}</td>
              <td>{produto.genero}</td>
              <td>{produto.quantidade_vendida}</td>
              <td>
              <Button onClick ={() => onDismiss(produto.id)}>
                Dismiss
              </Button>
              </td>
            </tr>
          ))}
        </tbody>
      </table>
  );
}

function Search(props) {
  const {searchTerm, handleInputChange, children} = props;
  return (
      <form>
        {children} <input type="text" placeholder="Search by produto name"
          name="searchTerm" value={searchTerm} 
          onChange={(event) => handleInputChange(event)}/>
      </form>
    );
}

function Button(props) {
  const {
    onClick,
    className='',
    children,
  } = props;

  return (
    <button
      onClick={onClick}
      className={className}
      type="button"
    >
      {children}
    </button>
  );
}

class App extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      list: null, 
      searchTerm: ''
    };
    this.onDismiss = this.onDismiss.bind(this);
    this.onSearchChange = this.onSearchChange.bind(this);
    this.handleInputChange = this.handleInputChange.bind(this);
  }

  handleInputChange(event) {
    const target = event.target;
    const value = 
      target.type === 'checkbox' ? target.checked : target.value;
    const name = target.name;

    this.setState({
      [name]: value
    });
    
  }

  componentDidMount() {
    fetch('https://polistore.onrender.com/api/v1/produtos/') 
      .then((response) => response.json())
      .then((result) => this.setState({ list: result }))
      .catch((error) => console.log(error));
  }

  onDismiss(id) {
    const updatedList = this.state.list.filter(item => item.id !== id);
    this.setState({ list: updatedList });
  }
  
  onSearchChange(event) {
    this.setState({ searchTerm: event.target.value });
}

render() {
  const {list, searchTerm} = this.state;
  
  return (
    <div className="App">
      <Search
        searchTerm={searchTerm}
        handleInputChange={(e) => this.handleInputChange(e)}
      >
        Search term:
      </Search>
      { list && (
        <ProdutosTable
          list={list} 
          searchTerm={searchTerm}
          onDismiss={(id) => this.onDismiss(id)}
        />
      )}
    </div>
  );
}
}
  


export default App
